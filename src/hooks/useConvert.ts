import { useEffect, useState } from "react";
import { useAppSelector } from "./redux";

export const useConvert = (amount: number, currency: string | undefined) => {
  const [convertedValue, setConvertedValue] = useState("");
  const currencyObject = useAppSelector(
    (state) => state.currencyDataReducer.currencyData.rates
  );

  useEffect(() => {
    if (amount && currency) {
      const res = amount * currencyObject[currency.toUpperCase()];
      setConvertedValue(res.toFixed(2));
    }
  }, [amount, currency]);

  return convertedValue;
};
