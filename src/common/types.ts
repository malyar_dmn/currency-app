export interface CurrencyDataState {
  currencyData: CurrencyDataObject;
  loading: boolean;
  error: null | string;
}

export interface CurrencyDataObject {
  rates: Record<string, number>;
}

export interface FetchCurrencyData {
  type: CurrencyActionTypes.FETCH_CURRENCY_DATA;
  payload: any;
}

export interface FetchCurrencyDataError {
  type: CurrencyActionTypes.FETCH_CURRENCY_DATA_ERROR;
  payload: string;
}

export enum CurrencyActionTypes {
  FETCH_CURRENCY_DATA = "FETCH_CURRENCY_DATA",
  FETCH_CURRENCY_DATA_ERROR = "FETCH_CURRENCY_DATA_ERROR",
}

export enum CurrencyNames {
  USD = "USD",
  EUR = "EUR",
  PLN = "PLN",
}

export type CurrencyDataAction = FetchCurrencyData | FetchCurrencyDataError;
