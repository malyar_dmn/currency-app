export const enumToArrayValues = <T>(e: { [s: string]: T }) => {
  return Object.entries(e).map(([key, value]) => value);
};
