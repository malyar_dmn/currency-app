import { Tab, Tabs } from "@mui/material";
import React, { useEffect } from "react";
import { Outlet, useNavigate, useParams } from "react-router-dom";
import { enumToArrayValues } from "../common/helpers";
import { CurrencyNames } from "../common/types";
import { useAppDispatch, useAppSelector } from "../hooks/redux";
import { fetchCurrencyData } from "../store/reducers/ActionCreators";

const currencies = enumToArrayValues(CurrencyNames);

export const ConverterPage = () => {
  const navigate = useNavigate();
  const { currency } = useParams();
  const dispatch = useAppDispatch();
  const { currencyData } = useAppSelector((state) => state.currencyDataReducer);

  useEffect(() => {
    dispatch(fetchCurrencyData());
  }, []);

  const onChangeTabHandler = (event: React.SyntheticEvent, newVal: string) => {
    navigate(`/converter/${newVal.toLowerCase()}`);
  };
  return (
    <>
      <Tabs
        value={currency?.toUpperCase()}
        onChange={onChangeTabHandler}
        centered
      >
        {currencies.map((tab) => (
          <Tab value={tab} label={`UAH - ${tab}`} key={tab} />
        ))}
      </Tabs>
      <Outlet />
    </>
  );
};
