import { Navigate, Route, Routes } from "react-router-dom";
import { CurrencyNames } from "../common/types";
import { Converter } from "../components/Converter/Converter";
import { ConverterPage } from "../pages/ConverterPage";

export const AppRouter = () => {
  return (
    <Routes>
      <Route path="converter" element={<ConverterPage />}>
        <Route path=":currency" element={<Converter />} />
      </Route>
      <Route
        path="*"
        element={
          <Navigate to={`/converter/${CurrencyNames.USD.toLowerCase()}`} />
        }
      />
    </Routes>
  );
};
