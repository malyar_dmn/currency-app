import { Button, Card, Grid, TextField } from "@mui/material";
import React, { useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { useAppSelector } from "../../hooks/redux";
import classes from "./Converter.module.css";
import { useConvert } from "../../hooks/useConvert";
import { ErrorMessage } from "../ErrorMessage/ErrorMessage";
import { Loader } from "../Loader/Loader";

export const Converter = () => {
  const [inputValue, setInputValue] = useState(0);
  const inputRef = useRef<HTMLInputElement>(null);
  const { currency } = useParams();
  const { loading, error } = useAppSelector(
    (state) => state.currencyDataReducer
  );
  const convertedValue = useConvert(inputValue, currency);

  const handleSubmit = (value: number) => {
    setInputValue(Number(value));
  };

  const handleClick = (e: React.MouseEvent) => {
    handleSubmit(Number(inputRef.current?.value || 0));
  };

  const handlePressEnter = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (["Enter", "NumpadEnter"].includes(e.code)) {
      handleSubmit(Number(e.target.value));
    }
  };
  if (loading) {
    return <Loader />;
  }
  if (error) {
    return <ErrorMessage msg={error} />;
  }
  return (
    <Card className={classes.cardHolder}>
      <Grid container spacing={1}>
        <Grid container item justifyContent="space-between" alignItems="center">
          <Grid item xs={8}>
            <TextField
              size="small"
              inputRef={inputRef}
              onKeyDown={handlePressEnter}
              type="number"
              inputProps={{
                step: "0.01",
                min: "0",
              }}
            />
          </Grid>
          <Grid item xs={4}>
            <span>
              = {convertedValue} {currency?.toUpperCase()}
            </span>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={handleClick}
            variant="contained"
            className={classes.submitButton}
          >
            Count
          </Button>
        </Grid>
      </Grid>
    </Card>
  );
};
