import { CircularProgress, Grid } from "@mui/material";

export const Loader = () => {
  return (
    <Grid container justifyContent="center">
      <CircularProgress sx={{ padding: 12 }} />
    </Grid>
  );
};
