import { Card, Grid, Typography } from "@mui/material";
import React, { FC } from "react";

interface Props {
  msg: string;
}

export const ErrorMessage: FC<Props> = (props) => {
  const { msg } = props;
  return (
    <Grid container justifyContent="center">
      <Card sx={{ maxWidth: 300, padding: 12, marginTop: 2 }}>
        <Typography color="red">{msg}</Typography>
      </Card>
    </Grid>
  );
};
