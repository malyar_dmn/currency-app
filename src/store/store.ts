import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import currencyDataReducer from "../store/reducers/currencyDataSlice";

const rootReducer = combineReducers({
  currencyDataReducer,
});

export const setupStore = () => {
  return configureStore({
    reducer: rootReducer,
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore["dispatch"];
