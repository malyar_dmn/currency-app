import axios from "axios";
import { AppDispatch } from "../store";
import { currencyDataSlice } from "./currencyDataSlice";

export const fetchCurrencyData = () => async (dispatch: AppDispatch) => {
  try {
    dispatch(currencyDataSlice.actions.fetchCurrencyData());
    const res = await axios.get(
      "https://api.exchangerate.host/latest?base=UAH"
    );
    dispatch(currencyDataSlice.actions.fetchCurrencyDataSuccess(res.data));
  } catch (e) {
    dispatch(
      currencyDataSlice.actions.fetchCurrencyDataError(
        "Something went wrong...=("
      )
    );
  }
};
