import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { CurrencyDataState } from "../../common/types";

const initialState: CurrencyDataState = {
  currencyData: { rates: {} },
  loading: false,
  error: null,
};

export const currencyDataSlice = createSlice({
  name: "currencyData",
  initialState,
  reducers: {
    fetchCurrencyData(state) {
      state.loading = true;
    },
    fetchCurrencyDataSuccess(state, action: PayloadAction<any>) {
      state.loading = false;
      state.error = "";
      state.currencyData = action.payload;
    },
    fetchCurrencyDataError(state, action: PayloadAction<string>) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export default currencyDataSlice.reducer;
